package main

import (
	"oreopon/promotion/controllers"
	"oreopon/promotion/entity"

	"github.com/gin-gonic/gin"
)

func main(){
router:=gin.Default()
entity.ConnectDatabase()
router.POST("/createNormalPromotion",controllers.CreatePromotion)
router.GET("/findPromotion",controllers.FindAllPromotion)
router.GET("/findPromotion/:id",controllers.FindPromotion)
router.PATCH("/findPromotion/:id", controllers.UpdatePromotion)
router.Run("localhost:8000")
}