package entity

import (
	"time"

	"github.com/google/uuid"
)

type Promotion struct {
	Promotion_ID uuid.UUID `gorm:"type:uuid;default:uuid_generate_v4();primary_key" json:"promotion_id"`
	Title string `json:"title" `
	StartDate string `json:"start_date" `
	EndDate string `json:"end_date" `
	PercentDiscount int `json:"percent_discount" `
	CreateBy string `json:"create_by" `
	UpdateBy string `json:"update_by" `
	CreatedDate time.Time `json:"created_date"`
	UpdatedDate time.Time `json:"updated_date"`
}