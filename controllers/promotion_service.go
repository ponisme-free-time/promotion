package controllers

import (
	"net/http"
	"oreopon/promotion/entity"
	"time"

	"github.com/gin-gonic/gin"
)

type CreateNormalPromotion struct{
	Title string `json:"title" binding:"required"`
	StartDate string `json:"start_date" binding:"required"`
	EndDate string `json:"end_date" binding:"required"`
	PercentDiscount int `json:"percent_discount" binding:"required"`

}
func CreatePromotion(c *gin.Context){
	var input CreateNormalPromotion


	if err := c.ShouldBindJSON(&input); err != nil{
		c.AbortWithStatusJSON(http.StatusBadRequest,gin.H{"error": err.Error()})
		return
	}
	loc, _ := time.LoadLocation("Asia/Bangkok")
	data := entity.Promotion{Title: input.Title, StartDate: input.StartDate,EndDate: input.EndDate,PercentDiscount: input.PercentDiscount,CreateBy: "ADMIN",UpdateBy: "ADMIN",CreatedDate: time.Now().In(loc),UpdatedDate: time.Now().In(loc)}
    entity.DB.Create(&data)
	c.JSON(http.StatusOK,gin.H{"data":input})
}

func FindAllPromotion(c *gin.Context){
	var promotions []entity.Promotion
	    entity.DB.Find(&promotions)

    c.JSON(http.StatusOK, gin.H{"data": promotions})
}

func FindPromotion(c *gin.Context) {
    var promotion entity.Promotion

    if err := entity.DB.Where("promotion_id = ?", c.Param("id")).First(&promotion).Error; err != nil {
        c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": err.Error()})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": promotion})
}

func UpdatePromotion(c *gin.Context) {
    var promotion entity.Promotion
    if err := entity.DB.Where("promotion_id = ?", c.Param("id")).First(&promotion).Error; err != nil {
        c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "record not found"})
        return
    }

    var input CreateNormalPromotion

    if err := c.ShouldBindJSON(&input); err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    updatedPost := entity.Promotion{Title: input.Title, StartDate: input.StartDate,EndDate:input.EndDate,PercentDiscount:input.PercentDiscount}

     entity.DB.Model(&promotion).Updates(&updatedPost)
    c.JSON(http.StatusOK, gin.H{"data": promotion})
}
